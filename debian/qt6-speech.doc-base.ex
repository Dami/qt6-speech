Document: qt6-speech
Title: Debian qt6-speech Manual
Author: <insert document author here>
Abstract: This manual describes what qt6-speech is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/qt6-speech/qt6-speech.sgml.gz

Format: postscript
Files: /usr/share/doc/qt6-speech/qt6-speech.ps.gz

Format: text
Files: /usr/share/doc/qt6-speech/qt6-speech.text.gz

Format: HTML
Index: /usr/share/doc/qt6-speech/html/index.html
Files: /usr/share/doc/qt6-speech/html/*.html
